# Installation

Install by npm:

```shell
npm install --save @do-while-for-each/context-side
```

or install with yarn:

```shell
yarn add @do-while-for-each/context-side
```
