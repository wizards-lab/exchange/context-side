export type ICtx = IWorkerCtx | IDedicatedWorkerGlobalScopeCtx | IWindowCtx;

/**
 * https://html.spec.whatwg.org/multipage/workers.html#dedicated-workers-and-the-worker-interface
 */
export interface IWorkerCtx {

  postMessage(message: any, transfer: Transferable[]): void;

  postMessage(message: any, options?: { transfer?: Transferable[]; }): void;

  terminate(): void;

}

/**
 * https://html.spec.whatwg.org/multipage/workers.html#dedicated-workers-and-the-dedicatedworkerglobalscope-interface
 */
export interface IDedicatedWorkerGlobalScopeCtx {

  postMessage(message: any, transfer: Transferable[]): void;

  postMessage(message: any, options?: { transfer?: Transferable[]; }): void;

  close(): void;

}

export interface IWindowCtx {

  postMessage(message: any, targetOrigin: string, transfer?: Transferable[]): void;

  postMessage(message: any, options?: { transfer?: Transferable[]; targetOrigin?: string; }): void;

}
