import {ContextSideAbstract} from './context-side.abstract';
import {IPostMessageData} from './contract';

/**
 * Built-in structural cloning is used.
 */
export class ContextSideSimple<TPost = any, TIncoming = any> extends ContextSideAbstract<TIncoming> {

  send(data: IPostMessageData<TPost>) {
    if (data.transfer)
      this.context.postMessage(data.message, data.transfer);
    else
      this.context.postMessage(data.message);
  }

  onIncoming(event: MessageEvent<TIncoming>): void {
    this.emit('incoming', event.data);
  }

}
