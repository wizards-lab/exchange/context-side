import {EventEmitter} from '@do-while-for-each/common';

export abstract class ContextSideAbstract<TIncoming = any>
  extends EventEmitter<{ incoming: TIncoming }> {

  protected constructor(protected readonly context: any,
                        protected readonly name = '') {
    super();
    this.context.addEventListener('message', this.onIncoming.bind(this));
    this.context.addEventListener('messageerror', this.onIncomingError.bind(this));
    this.logPrefix = this.name ? `[${this.name}]:` : `${this.context}:`;
  }

  dispose(): void {
    this.context.removeEventListener('message', this.onIncoming);
    this.context.removeEventListener('messageerror', this.onIncomingError);
    super.dispose();
  }

  abstract send(data: any): void;

  abstract onIncoming(event: MessageEvent): void;

  protected onIncomingError(event: MessageEvent): void {
    this.logError('Error receiving from "messageerror" listener:', event);
  }

//region Log

  logPrefix: string;

  logError(...args: any[]) {
    console.error(this.logPrefix, ...args);
  }

//endregion Log

}
