export interface IPostMessageData<TPost = any> {
  message: TPost;
  transfer?: Transferable[];
  targetOrigin?: string; // globalThis.origin
}


export interface IConverter<TSend = any, TPost = any, TRead = any, TIncoming = any> {

  write(data: TSend): IPostMessageData<TPost>;

  read(event: MessageEvent<TRead>): TIncoming;

}
