import {ContextSideAbstract} from './context-side.abstract';
import {IConverter} from './contract';

export class ContextSideConverted<TSend = any, TPost = any, TRead = any, TIncoming = any>
  extends ContextSideAbstract<TIncoming> {

  constructor(context: any,
              name = '',
              private converter: IConverter<TSend, TPost, TRead, TIncoming>) {
    super(context, name);
  }

  send(data: TSend) {
    const converted = this.converter.write(data); // TSend -> TPost
    if (converted.transfer)
      this.context.postMessage(converted.message, converted.transfer);
    else
      this.context.postMessage(converted.message);
  }

  onIncoming(event: MessageEvent<TRead>): void {
    const converted = this.converter.read(event); // TRead -> TReceived
    this.emit('incoming', converted);
  }

}
