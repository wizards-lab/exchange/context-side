import {ContextSideAbstract} from './context-side.abstract';
import {IPostMessageData} from './contract';

export class ContextSideWindow<TPost = any, TIncoming = any> extends ContextSideAbstract<TIncoming> {

  send(data: IPostMessageData<TPost>) {
    this.context.postMessage(
      data,
      data.targetOrigin || globalThis.origin,
      data.transfer || []
    );
  }

  onIncoming(event: MessageEvent<TIncoming>): void {
    if (event.origin !== globalThis.origin) {
      return;
    }
    this.emit('incoming', event.data);
  }

}
